<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', function () {
    return [
            'status' => 200,
            'title' => 'core laravel 6.8 login admin',
            'use_name' => 'Vũ Văn Hải dev'
    ];
});


Route::group(['prefix'=>'admin/login','middleware'=>'CheckLogin'],function(){
    Route::get('/', 'Admin\LoginController@getLogin')->name('admin.login');
    Route::post('/', 'Admin\LoginController@postLogin')->name('admin.post.login');
});

Route::group(['prefix'=>'admin','middleware'=>'CheckAdmin'],function(){
    Route::get('/logout', 'Admin\LoginController@getLogout')->name('admin.logout');
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');

    // Route danh mục cha
    Route::resource('category', 'Admin\CategoryController');
    Route::get('/delete/{id}','Admin\CategoryController@delete');
    // Route danh mục con
    Route::get('/child/category/{id}','Admin\CategoryController@indexCateChild');
    Route::get('/child/category/add/{id}','Admin\CategoryController@createCateChild');
    Route::post('/child/category/add/{id}','Admin\CategoryController@storeCateChild');
    Route::get('/child/category/edit/{id}','Admin\CategoryController@showCateChild');
    Route::post('/child/category/edit/{id}','Admin\CategoryController@editCateChild');
    Route::get('/child/category/delete/{id}','Admin\CategoryController@deleteCateChild');

});




//route frontend
Route::group(['middleware'=>'compressHTML'],function(){
    // Route::get('/', 'Web\HomeController@getHome');
});


// dua tat ca route khong co ve trang chu
Route::any('{all}', function(){
    return redirect('/');
})->where('all', '.*');