<?php


namespace App\Http\Middleware;

use Closure;

class CompressResponseMiddleware
{
    public function handle($request, Closure $next)
    {
      
        $response = $next($request);
        $buffer = $response->getContent();
        $buffer = clearSpaceBuffer($buffer);
        $response->setContent($buffer);
        ini_set('zlib.output_compression', 'On');
        return $response;
    }

}
function clearSpaceBuffer($buffer)
{
    if (env('APP_ENV')=='local'){
        return $buffer;
    }
    $arrStr = array(chr(9), chr(10));
    $buffer = str_replace($arrStr, "", $buffer);
    $buffer = preg_replace('!\s+!', ' ', $buffer);
   // $buffer = str_replace("> <", "><", $buffer);
    return trim($buffer);
}