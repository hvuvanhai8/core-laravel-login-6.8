$('.single-item').owlCarousel({
    loop:true,
    margin:0,
    dots:false,
    nav:false,
    autoplay:false,
    autoplaySpeed: 2000,
    autoTimeout: 4000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
});


$('.slide_hot_tour').owlCarousel({
    loop:true,
    margin:12,
    dots:true,
    nav:true,
    autoplay:false,
    autoplaySpeed: 2000,
    autoTimeout: 4000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:5
        }
    }
});