<ul class="sidebar-menu" data-widget="tree">
    <li class="treeview menu-open">
        <a href="#">
        <i class="fa fa-dashboard"></i> <span>Thống kê</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="/dashboard"><i class="fa fa-circle-o"></i> Bảng thống kê</a></li>
        </ul>
    </li>
    <li class="treeview">
        <a href="#">
        <i class="fa fa-fw fa-laptop"></i>
        <span>Quản lý danh mục</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
        </a>
        <ul class="treeview-menu">
        <li><a href="/admin/category"><i class="fa fa-fw fa-list-ul"></i> Danh sách danh mục</a></li>
        <li><a href="/admin/category/create"><i class="fa fa-fw fa-plus-square-o"></i> Thêm danh mục</a></li>
        </ul>
    </li>
</ul>