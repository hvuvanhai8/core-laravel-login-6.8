<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    //
    public function getLogin(){
        return view('admin.login');
    }

    public function postLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect('/admin/dashboard');
        }
        else{
            return redirect('/admin/login')->with(['error'=>'Email khoặc mật khẩu không đúng ']);
        }



    }

    public function getLogout(Request $request)
    {
        Auth::logout();
        return redirect('/admin/login');
    }
}
