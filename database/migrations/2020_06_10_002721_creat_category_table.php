<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cat_name')->nullable();
            $table->string('cat_rewrite')->nullable();
            $table->string('cat_slug')->nullable();
            $table->string('cat_type')->nullable();
            $table->string('cat_active')->nullable();
            $table->string('cat_parent_id')->nullable();
            $table->string('cat_all_child')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
